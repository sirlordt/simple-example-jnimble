package simple.example.jnimble.common;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Utilities {

	private Utilities() {
		
		//Prevent instances from this class
		
	}
	
    public static boolean isNotNullAndEmpty( final String strData ) {

        return strData != null && strData.isEmpty() == false;

    }

    public static boolean isNullOrEmpty( final String strData ) {

        return strData == null || strData.isEmpty();

    }
	
    public static LocalDate parseLocalDate( final String strDate,
                                            String strDateFormat ) {

        LocalDate result = null;

        try {

            if ( isNotNullAndEmpty( strDate ) ) {

                if ( isNullOrEmpty( strDateFormat ) ) {

                    strDateFormat = Constants._Date_Format; //"dd/MM/yyyy";

                }

                final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern( strDateFormat, 
                																	     Locale.US );

                result = LocalDate.parse( strDate, 
                						  dateTimeFormatter );

            }

        }
        catch ( final Exception ex ) {

        	//
        	
        }

        return result;

    }
    
    
    public static LocalTime parseLocalTime( final String strTime, 
    										String strTimeFormat ) {

        LocalTime result = null;

        try {

            if ( isNotNullAndEmpty( strTime ) ) {

                if ( isNullOrEmpty( strTimeFormat ) ) {

                    strTimeFormat = Constants._Time_Format_24; //"HH:mm:ss";

                }

                final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern( strTimeFormat, 
                																		 Locale.US );

                result = LocalTime.parse( strTime.toUpperCase(), 
                						  dateTimeFormatter );

            }

        }
        catch ( final Exception ex ) {
        	
        	//

        }

        return result;

    }
    
}
