package simple.example.jnimble.common;

public class Constants {

	private Constants() {
		
		//Prevent instances from this class
		
	}

    /** The Constant _ISO_Date_Format. */
    public static final String _ISO_Date_Format = "yyyy-MM-dd";

    /** The Constant _Date_Time_Format_24. */
    public static final String _ISO_Date_And_Time_Format_24 = "yyyy-MM-dd HH:mm:ss";

    /** The Constant _Date_Format. */
    public static final String _Date_Format = "dd/MM/yyyy";

    /** The Constant _Date_Format_File_System. */
    public static final String _Date_Format_File_System = "dd-MM-yyyy";

    /** The Constant _Time_Format_12. */
    public static final String _Time_Format_12 = "hh:mm:ss a";

    /** The Constant _Time_Format_24. */
    public static final String _Time_Format_24 = "HH:mm:ss";

    /** The Constant _Time_Format_24. */
    public static final String _Time_Format_24_Weak = "H:m:s";

    /** The Constant _Time_Format_File_System_12. */
    public static final String _Time_Format_File_System_12 = "hh-mm-ss a";

    /** The Constant _Time_Format_File_System_24. */
    public static final String _Time_Format_File_System_24 = "HH-mm-ss";
	
}
