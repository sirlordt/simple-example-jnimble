package simple.example.jnimble.init;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import net.nimble.NbConnection;
import net.nimble.NbRow;
import net.nimble.Nimble;
import net.nimble.sql.SqlDialect;
import simple.example.jnimble.common.Utilities;
import simple.example.jnimble.models.TblLogEntryV1;
import simple.example.jnimble.models.TblLogEntryV1CallStack;
import simple.example.jnimble.models.TblLogEntryV1Head;
import simple.example.jnimble.models.TblWebSocketMessageQueue;

public class Application {

	public static DataSource getDataSource() {
	
		Properties properties = new Properties();
		
		FileInputStream fileInputStream = null;
		
		BasicDataSource dataSource = null;
		
		try {
		
			fileInputStream = new FileInputStream( "config/db.properties" );
			
			properties.load(fileInputStream);
			
			dataSource = new BasicDataSource();
			
			dataSource.setDriverClassName( properties.getProperty( "DB_DRIVER_CLASS" ) );
			
			String strURLParams = "";
			
			if ( properties.getProperty( "DB_URL_PARAMS" ) != null ) {
				
				strURLParams = "?" + properties.getProperty( "DB_URL_PARAMS" ).replaceAll( Pattern.quote( "{#currentTimeZone#}" ), TimeZone.getDefault().getID() );
				
			}
			
	        dataSource.setUrl( properties.getProperty( "DB_URL" ) + strURLParams );
	        dataSource.setUsername( properties.getProperty( "DB_USERNAME" ) );
	        dataSource.setPassword( properties.getProperty( "DB_PASSWORD" ) );
	        dataSource.setDefaultAutoCommit( false );
		
		}
		catch( Exception ex ){
			
			ex.printStackTrace();
			
		}
        
		return dataSource;
		
	}	
	
	public static void main(String[] args) throws SQLException {

		DataSource dataSource = getDataSource();
		
		Nimble nimble = new Nimble( dataSource, SqlDialect.MYSQL );
	    
        try( NbConnection connection = nimble.getConnection() ) {
	        
        	connection.setAutoCommit( false );
        	connection.setTransactionIsolation( NbConnection.TRANSACTION_READ_COMMITTED );
        	
        	// working with connection
         	//Savepoint savepoint = 
        	connection.setSavepoint();
            
        	long longRow = 0;
        	//long longPage = 0;
        	        	
        	try {
                
        		do {
	        		
	        		//Replace with you table name
        			// Limit" + Long.toString( longPage * 99 ) + ", 100"
        												
	        		NbRow[] rowList = connection.query( "Select * From tblWebSocketMessageQueue Where Type='loggerRecordV1' Order By CreatedAt Limit 0, 100" ).fetchRowList();
	        		
	        		for ( NbRow row : rowList ) {
    	                
	        			final JSONObject jsonMessage = new JSONObject( row.getString( "Message" ) );
	        			
	        			TblLogEntryV1Head tblLogEntryV1Head = new TblLogEntryV1Head();
	        			
	        			tblLogEntryV1Head.loadFromJSON( jsonMessage );
	        			
	        			String strLogEntryV1HeadId = connection.query( "Select A.Id From tblLogEntryV1Head As A Where A.DataChecksum = '" + tblLogEntryV1Head.getDataChecksum() + "'" ).fetchValue( String.class );
	        			
	        			String strLogEntryV1Id = null;
	        			
	        			if ( Utilities.isNullOrEmpty( strLogEntryV1HeadId ) ) {

		        			connection.insert( tblLogEntryV1Head );
		        			
		        			strLogEntryV1HeadId = tblLogEntryV1Head.getId();
		        			
	        			}
        				
        				jsonMessage.put( "LogEntryV1HeadId", strLogEntryV1HeadId );

        				//Detect CallStack
                        JSONObject jsonLogEntryStackTrace = null;
	        			
	        			if ( jsonMessage.has( "Thrown" ) ) {

                            jsonLogEntryStackTrace = jsonMessage.getJSONObject( "Thrown" );
	        				
	        			}

	        			if ( jsonMessage.has( "CallStack" ) ) {
	        				
                            jsonLogEntryStackTrace = jsonMessage.getJSONObject( "CallStack" );
	        				
	        			}
	        			//Detect CallStack
        				
        				TblLogEntryV1 tblLogEntryV1 = new TblLogEntryV1();
        				
        				tblLogEntryV1.loadFromJSON( jsonMessage );

        				if ( jsonLogEntryStackTrace != null &&
        				   	 jsonLogEntryStackTrace.has( "ThrownClassName" ) ) {
        					
        					String strThrownClassName = jsonLogEntryStackTrace.getString( "ThrownClassName" );
        					
        					if ( Utilities.isNotNullAndEmpty( strThrownClassName ) ) {
        					
        						tblLogEntryV1.setThrownClassName( strThrownClassName );
        					
        					}
        					
        				}
        				
	        			strLogEntryV1Id = connection.query( "Select A.Id From tblLogEntryV1 As A Where A.Id = '" + tblLogEntryV1.getId() + "'" ).fetchValue( String.class );
        				
	        			if ( Utilities.isNullOrEmpty( strLogEntryV1Id ) ) {
	        			
	        				connection.insert( tblLogEntryV1 );

	        				strLogEntryV1Id = tblLogEntryV1Head.getId();
	        				
	        			}
	        			
	        			if ( jsonLogEntryStackTrace != null ) {
	        				
                            final JSONArray jsonLogEntryStackTraceArray = jsonLogEntryStackTrace.getJSONArray( "StackTrace" );

                            for ( int intStackTraceIndex = 0; intStackTraceIndex < jsonLogEntryStackTraceArray.length(); intStackTraceIndex++ ) {
                            	
    	        				TblLogEntryV1CallStack tblLogEntryV1CallStack = new TblLogEntryV1CallStack();

    	        				JSONObject jsonCallStack = jsonLogEntryStackTraceArray.getJSONObject( intStackTraceIndex );
    	        				
    	        				jsonCallStack.put( "LogEntryV1Id", strLogEntryV1Id );
    	        				jsonCallStack.put( "CallStackOrder", intStackTraceIndex );
    	        				
    	        				tblLogEntryV1CallStack.loadFromJSON( jsonCallStack );
                            	
    	        				connection.insert( tblLogEntryV1CallStack );
    	        				
                            }
	        				
	        			}
	        			
	        			//System.out.println( "Id: " + row.getString( "Id" ) );
	        			//System.out.println( "Worker name: " + row.getString( "WorkerName" ) );
	        			
	        			//System.out.println( "Row: " + Long.toString( longRow ) );
	                    
	        			longRow = longRow + 1;
        				
	        			connection.delete( row.getString( "Id" ), TblWebSocketMessageQueue.class );
	        			
	        			// working with connection
	                    connection.commit();
	        			
	                    //connection.releaseSavepoint( savepoint );
	                    
	                }        		
        		
	        		//longPage += 1;
	        		
	        		System.gc();
	        		
	        		Thread.sleep( 2000 ); //Stop by 2 seconds
	        		
        		} while ( true );
                
            } 
            catch ( Exception ex ) {
                
            	ex.printStackTrace();
            	
            	connection.rollback();
                
            }        	
        	
	    }
		
	}

}
