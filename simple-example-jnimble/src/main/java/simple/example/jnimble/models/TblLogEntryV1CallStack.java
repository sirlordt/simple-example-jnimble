package simple.example.jnimble.models;

import java.time.LocalDate;
import java.time.LocalTime;

import org.codehaus.jettison.json.JSONObject;

import net.nimble.annotations.Column;
import net.nimble.annotations.Id;
import net.nimble.annotations.Table;
import simple.example.jnimble.common.Constants;
import simple.example.jnimble.common.Utilities;

@Table( "tblLogEntryV1CallStack" )
public class TblLogEntryV1CallStack {

	@Id
    @Column( "LogEntryV1Id" )
    private String strLogEntryV1Id;

	@Id
    @Column( "CallStackOrder" )
    private int intCallStackOrder;

    @Column( "PackageName" )
    private String strPackageName;

    @Column( "ClassName" )
    private String strClassName;

    @Column( "MethodName" )
    private String strMethodName;

    @Column( "Line" )
    private int intLine;

    @Column( "CreatedBy" )
    protected String strCreatedBy;

    @Column( "CreatedAtDate" )
    protected LocalDate createdAtDate;

    @Column( "CreatedAtTime" )
    protected LocalTime createdAtTime;

    public String getLogEntryV1Id() {

        return strLogEntryV1Id;

    }

    public void setLogEntryV1Id( final String strLogEntryV1Id ) {

        this.strLogEntryV1Id = strLogEntryV1Id;

    }

    public int getCallStackOrder() {

        return intCallStackOrder;

    }

    public void setCallStackOrder( final int intCallStackOrder ) {

        this.intCallStackOrder = intCallStackOrder;

    }

    public String getClassName() {

        return strClassName;

    }

    public void setClassName( final String strClassName ) {

        this.strClassName = strClassName;

    }

    public String getPackageName() {

        return strPackageName;

    }

    public void setPackageName( final String strPackageName ) {

        this.strPackageName = strPackageName;

    }

    public String getMethodName() {

        return strMethodName;

    }


    public void setMethodName( final String strMethodName ) {

        this.strMethodName = strMethodName;

    }

    public int getLine() {

        return intLine;

    }

    public void setLine( final int intLine ) {

        this.intLine = intLine;

    }
    
    public String getCreatedBy() {

        return strCreatedBy;

    }

    public void setCreatedBy( final String strCreatedBy ) {

        this.strCreatedBy = strCreatedBy;

    }

    public LocalDate getCreatedAtDate() {

        return createdAtDate;

    }

    public void setCreatedAtDate( final LocalDate createdAtDate ) {

        this.createdAtDate = createdAtDate;

    }

    public LocalTime getCreatedAtTime() {

        return createdAtTime;

    }

    public void setCreatedAtTime( final LocalTime createdAtTime ) {

        this.createdAtTime = createdAtTime;

    }
        
    public void loadFromJSON( final JSONObject jsonData ) {
    	
    	try {
    		
            strLogEntryV1Id = jsonData.has( "LogEntryV1Id" ) ? jsonData.getString( "LogEntryV1Id" ) : "";
            intCallStackOrder = jsonData.has( "CallStackOrder" ) ? jsonData.getInt( "CallStackOrder" ) : 0;
            strPackageName = jsonData.has( "PackageName" ) ? jsonData.getString( "PackageName" ) : "";
            strClassName = jsonData.has( "ClassName" ) ? jsonData.getString( "ClassName" ) : null;
            strMethodName = jsonData.has( "MethodName" ) ? jsonData.getString( "MethodName" ) : "";
            intLine = jsonData.has( "Line" ) ? jsonData.getInt( "Line" ) : 0;
            strCreatedBy = jsonData.has( "CreatedBy" ) ? jsonData.getString( "CreatedBy" ) : "";;
            createdAtDate = jsonData.has( "CreatedAtDate" ) ? Utilities.parseLocalDate( jsonData.getString( "CreatedAtDate" ), Constants._ISO_Date_Format ) : null;
            createdAtTime = jsonData.has( "CreatedAtTime" ) ? Utilities.parseLocalTime( jsonData.getString( "CreatedAtTime" ), Constants._Time_Format_24 ) : null;
            
            if ( Utilities.isNullOrEmpty( strCreatedBy ) ) {
            	
            	strCreatedBy = "process_log";
            	
            }

            if ( createdAtDate == null ) {
            
            	createdAtDate = LocalDate.now();
        		
        	}
            
            if ( createdAtTime == null ) {
            	
            	createdAtTime = LocalTime.now();
        	
            }

    	}
    	catch ( final Exception ex ) {
    		
    		//
    		
    	}
    	
    }

}
