package simple.example.jnimble.models;

import java.time.LocalDate;
import java.time.LocalTime;

import org.codehaus.jettison.json.JSONObject;

import net.nimble.annotations.Column;
import net.nimble.annotations.Id;
import net.nimble.annotations.Table;
import simple.example.jnimble.common.Constants;
import simple.example.jnimble.common.Utilities;

@Table( "tblLogEntryV1" )
public class TblLogEntryV1 {

	@Id
	@Column( "Id" )
	String strId;

    @Column( "LogEntryV1HeadId" )
    private String strLogEntryV1HeadId;

    @Column( "Type" )
    private String strType;

    @Column( "Level" )
    private String strLevel;

    @Column( "LoggedAtDate" )
    private LocalDate loggedAtDate;

    @Column( "LoggedAtTime" )
    private LocalTime loggedAtTime;

    @Column( "Sequence" )
    private long longSequence;

    @Column( "ThreadId" )
    private long longThreadId;

    @Column( "ThreadName" )
    private String strThreadName;

    @Column( "PackageName" )
    private String strPackageName;

    @Column( "ClassName" )
    private String strClassName;

    @Column( "MethodName" )
    private String strMethodName;

    @Column( "Line" )
    private int intLine;

    @Column( "Code" )
    private String strCode;

    @Column( "Message" )
    private String strMessage;

    @Column( "Data" )
    private String strData;

    @Column( "ThrownClassName" )
    private String strThrownClassName;

    @Column( "StackTraceCount" )
    private int intStackTraceCount;

    @Column( "CreatedBy" )
    protected String strCreatedBy;

    @Column( "CreatedAtDate" )
    protected LocalDate createdAtDate;

    @Column( "CreatedAtTime" )
    protected LocalTime createdAtTime;

    public String getId() {

        return strId;

    }

    public void setId( final String strId ) {

        this.strId = strId;

    }

    public String getLogEntryV1HeadId() {

        return strLogEntryV1HeadId;

    }

    public void setLogEntryV1HeadId( final String strLogEntryV1HeadId ) {

        this.strLogEntryV1HeadId = strLogEntryV1HeadId;

    }

    public String getType() {

        return strType;

    }

    public void setType( final String strType ) {

        this.strType = strType;

    }

    public String getLevel() {

        return strLevel;

    }

    public void setLevel( final String strLevel ) {

        this.strLevel = strLevel;

    }

    public LocalDate getLoggedAtDate() {

        return loggedAtDate;

    }

    public void setLoggedAtDate( final LocalDate loggedAtDate ) {

        this.loggedAtDate = loggedAtDate;

    }

    public LocalTime getLoggedAtTime() {

        return loggedAtTime;

    }

    public void setLoggedAtTime( final LocalTime loggedAtTime ) {

        this.loggedAtTime = loggedAtTime;

    }

    public long getSequence() {

        return longSequence;

    }

    public void setSequence( final long longSequence ) {

        this.longSequence = longSequence;

    }

    public long getThreadId() {

        return longThreadId;

    }

    public void setThreadId( final long longThreadId ) {

        this.longThreadId = longThreadId;

    }

    public String getClassName() {

        return strClassName;

    }

    public void setClassName( final String strClassName ) {

        this.strClassName = strClassName;

    }

    public String getThreadName() {

        return strThreadName;

    }

    public void setThreadName( final String strThreadName ) {

        this.strThreadName = strThreadName;

    }

    public String getPackageName() {

        return strPackageName;

    }

    public void setPackageName( final String strPackageName ) {

        this.strPackageName = strPackageName;

    }

    public String getMethodName() {

        return strMethodName;

    }


    public void setMethodName( final String strMethodName ) {

        this.strMethodName = strMethodName;

    }

    public int getLine() {

        return intLine;

    }

    public void setLine( final int intLine ) {

        this.intLine = intLine;

    }

    public String getCode() {

        return strCode;

    }

    public void setCode( final String strCode ) {

        this.strCode = strCode;

    }

    public String getMessage() {

        return strMessage;

    }

    public void setMessage( final String strMessage ) {

        this.strMessage = strMessage;

    }

    public String getData() {

        return strData;

    }

    public void setData( final String strData ) {

        this.strData = strData;

    }

    public String getThrownClassName() {

        return strThrownClassName;

    }

    public void setThrownClassName( final String strThrownClassName ) {

        this.strThrownClassName = strThrownClassName;

    }

    public int getStackTraceCount() {

        return intStackTraceCount;

    }

    public void setStackTraceCount( final int intStackTraceCount ) {

        this.intStackTraceCount = intStackTraceCount;

    }
    
    public String getCreatedBy() {

        return strCreatedBy;

    }

    public void setCreatedBy( final String strCreatedBy ) {

        this.strCreatedBy = strCreatedBy;

    }

    public LocalDate getCreatedAtDate() {

        return createdAtDate;

    }

    public void setCreatedAtDate( final LocalDate createdAtDate ) {

        this.createdAtDate = createdAtDate;

    }

    public LocalTime getCreatedAtTime() {

        return createdAtTime;

    }

    public void setCreatedAtTime( final LocalTime createdAtTime ) {

        this.createdAtTime = createdAtTime;

    }
    
    public void loadFromJSON( final JSONObject jsonData ) {
    	
    	try {
    	
            strId = jsonData.has( "Id" ) ? jsonData.getString( "Id" ) : "";
            strLogEntryV1HeadId = jsonData.has( "LogEntryV1HeadId" ) ? jsonData.getString( "LogEntryV1HeadId" ) : "";
            strType = jsonData.has( "LogType" ) ? jsonData.getString( "LogType" ) : "";
            strLevel = jsonData.has( "Level" ) ? jsonData.getString( "Level" ) : "";
            loggedAtDate = jsonData.has( "Date" ) ? Utilities.parseLocalDate( jsonData.getString( "Date" ), Constants._ISO_Date_Format ) : null;
            loggedAtTime = jsonData.has( "Time" ) ? Utilities.parseLocalTime( jsonData.getString( "Time" ), Constants._Time_Format_24 ) : null;
            longSequence = jsonData.has( "Sequence" ) ? jsonData.getLong( "Sequence" ) : 0;
            longThreadId = jsonData.has( "ThreadId" ) ? jsonData.getLong( "ThreadId" ) : 0;
            strThreadName = jsonData.has( "ThreadName" ) ? jsonData.getString( "ThreadName" ) : "";
            strPackageName = jsonData.has( "PackageName" ) ? jsonData.getString( "PackageName" ) : "";
            strClassName = jsonData.has( "ClassName" ) ? jsonData.getString( "ClassName" ) : "";;
            strMethodName = jsonData.has( "MethodName" ) ? jsonData.getString( "MethodName" ) : "";
            intLine = jsonData.has( "Line" ) ? jsonData.getInt( "Line" ) : 0;
            strCode = jsonData.has( "Code" ) ? jsonData.getString( "Code" ) : "";
            strMessage = jsonData.has( "Message" ) ? jsonData.getString( "Message" ) : "";
            strData = jsonData.has( "Data" ) ? jsonData.getString( "Data" ) : "";
            strThrownClassName = jsonData.has( "ThrownClassName" ) ? jsonData.getString( "ThrownClassName" ) : "";
            intStackTraceCount = jsonData.has( "StackTraceCount" ) ? jsonData.getInt( "StackTraceCount" ) : 0;
            strCreatedBy = jsonData.has( "CreatedBy" ) ? jsonData.getString( "CreatedBy" ) : "";;
            createdAtDate = jsonData.has( "CreatedAtDate" ) ? Utilities.parseLocalDate( jsonData.getString( "CreatedAtDate" ), Constants._ISO_Date_Format ) : null;
            createdAtTime = jsonData.has( "CreatedAtTime" ) ? Utilities.parseLocalTime( jsonData.getString( "CreatedAtTime" ), Constants._Time_Format_24 ) : null;
            
            if ( Utilities.isNullOrEmpty( strCreatedBy ) ) {
            	
            	strCreatedBy = "process_log";
            	
            }

            if ( createdAtDate == null ) {
            
            	createdAtDate = LocalDate.now();
        		
        	}
            
            if ( createdAtTime == null ) {
            	
            	createdAtTime = LocalTime.now();
        	
            }
    		
    	}
    	catch ( final Exception ex ) {
    		
    		
    	}
    	
    }
    
}
