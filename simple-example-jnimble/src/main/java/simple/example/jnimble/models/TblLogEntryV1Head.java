package simple.example.jnimble.models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.zip.CRC32;

import org.codehaus.jettison.json.JSONObject;

import net.nimble.annotations.Column;
import net.nimble.annotations.Id;
import net.nimble.annotations.Table;
import simple.example.jnimble.common.Constants;
import simple.example.jnimble.common.Utilities;

@Table( "tblLogEntryV1Head" )
public class TblLogEntryV1Head {

    @Id
    @Column( "Id" )
    private String strId;

    @Column( "LogProjectId" )
    private String strLogProjectId;

    @Column( "RunMark" )
    private String strRunMark;

    @Column( "DeploySite" )
    private String strDeploySite;

    @Column( "DeployNode" )
    private String strDeployNode;

    @Column( "DeployAppName" )
    private String strDeployAppName;

    @Column( "DeployAppVersion" )
    private String strDeployAppVersion;

    @Column( "DeployContext" )
    private String strDeployContext;

    @Column( "LogDataPath" )
    private String strLogDataPath;

    @Column( "LogDataName" )
    private String strLogDataName;

    @Column( "InstanceId" )
    private String strInstanceId;

    @Column( "LoggerName" )
    private String strLoggerName;

    @Column( "DataChecksum" )
    private String strDataChecksum;

    @Column( "CreatedBy" )
    protected String strCreatedBy;

    @Column( "CreatedAtDate" )
    protected LocalDate createdAtDate;

    @Column( "CreatedAtTime" )
    protected LocalTime createdAtTime;

    public String getId() {

        return strId;

    }

    public void setId( final String strId ) {

        this.strId = strId;

    }

    public String getLogProjectId() {

        return strLogProjectId;

    }

    public void setLogProjectId( final String strLogProjectId ) {

        this.strLogProjectId = strLogProjectId;

    }

    public String getRunMark() {

        return strRunMark;

    }

    public void setRunMark( final String strRunMark ) {

        this.strRunMark = strRunMark;

    }

    public String getDeploySite() {

        return strDeploySite;

    }

    public void setDeploySite( final String strDeploySite ) {

        this.strDeploySite = strDeploySite;

    }

    public String getDeployNode() {

        return strDeployNode;

    }

    public void setDeployNode( final String strDeployNode ) {

        this.strDeployNode = strDeployNode;

    }

    public String getDeployAppName() {

        return strDeployAppName;

    }

    public void setDeployAppName( final String strDeployAppName ) {

        this.strDeployAppName = strDeployAppName;

    }

    public String getDeployAppVersion() {

        return strDeployAppVersion;

    }

    public void setDeployAppVersion( final String strDeployAppVersion ) {

        this.strDeployAppVersion = strDeployAppVersion;

    }

    public String getDeployContext() {

        return strDeployContext;

    }


    public void setDeployContext( final String strDeployContext ) {

        this.strDeployContext = strDeployContext;

    }

    public String getLogDataPath() {

        return strLogDataPath;

    }

    public void setLogDataPath( final String strLogDataPath ) {

        this.strLogDataPath = strLogDataPath;

    }

    public String getLogDataName() {

        return strLogDataName;

    }

    public void setLogDataName( final String strLogDataName ) {

        this.strLogDataName = strLogDataName;

    }

    public String getInstanceId() {

        return strInstanceId;

    }

    public void setInstanceId( final String strInstanceId ) {

        this.strInstanceId = strInstanceId;

    }

    public String getLoggerName() {

        return strLoggerName;

    }

    public void setLoggerName( final String strLoggerName ) {

        this.strLoggerName = strLoggerName;

    }

    public String getDataChecksum() {

        return strDataChecksum;

    }

    public void setDataChecksum( final String strDataChecksum ) {

        this.strDataChecksum = strDataChecksum;

    }
    
    public String getCreatedBy() {

        return strCreatedBy;

    }

    public void setCreatedBy( final String strCreatedBy ) {

        this.strCreatedBy = strCreatedBy;

    }

    public LocalDate getCreatedAtDate() {

        return createdAtDate;

    }

    public void setCreatedAtDate( final LocalDate createdAtDate ) {

        this.createdAtDate = createdAtDate;

    }

    public LocalTime getCreatedAtTime() {

        return createdAtTime;

    }

    public void setCreatedAtTime( final LocalTime createdAtTime ) {

        this.createdAtTime = createdAtTime;

    }

    public String calcDataCheckSum() {
    	
    	String strResult = null;

    	try {

    		final String strDataToHash = strLogProjectId + ":" + 
    									 strRunMark + ":" + 
    									 strDeploySite + ":" + 
    									 strDeployNode + ":" + 
    									 strDeployAppName + ":" + 
    									 strDeployAppVersion + ":" + 
    									 strDeployContext + ":" + 
    									 strLogDataPath + ":" + 
    									 strLogDataName + ":" + 
    									 strInstanceId + ":" + 
    									 strLoggerName;
    		
            final CRC32 crc32 = new CRC32();

            crc32.reset();

            crc32.update( strDataToHash.getBytes() );

            strResult = Long.toHexString( crc32.getValue() );
    		
    	}	
		catch ( final Exception ex ) {
			
			//
			
		}
    	
    	return strResult;
    	
    }    
    
    public void loadFromJSON( final JSONObject jsonData ) {
    	
    	try {
    		
            strId = jsonData.has( "Id" ) ? jsonData.getString( "Id" ) : "";
            strLogProjectId = jsonData.has( "ProjectId" ) ? jsonData.getString( "ProjectId" ) : "";
            strRunMark = jsonData.has( "RunMark" ) ? jsonData.getString( "RunMark" ) : "";
            strDeploySite = jsonData.has( "DeploySite" ) ? jsonData.getString( "DeploySite" ) : "";
            strDeployNode = jsonData.has( "DeployNode" ) ? jsonData.getString( "DeployNode" ) : "";
            strDeployAppName = jsonData.has( "DeployAppName" ) ? jsonData.getString( "DeployAppName" ) : "";
            strDeployAppVersion = jsonData.has( "DeployAppVersion" ) ? jsonData.getString( "DeployAppVersion" ) : "";
            strDeployContext = jsonData.has( "DeployContext" ) ? jsonData.getString( "DeployContext" ) : "";
            strLogDataPath = jsonData.has( "LogDataPath" ) ? jsonData.getString( "LogDataPath" ) : "";
            strLogDataName = jsonData.has( "LogDataName" ) ? jsonData.getString( "LogDataName" ) : "";
            strInstanceId = jsonData.has( "InstanceId" ) ? jsonData.getString( "InstanceId" ) : "";;
            strLoggerName = jsonData.has( "LoggerName" ) ? jsonData.getString( "LoggerName" ) : "";
            strDataChecksum = jsonData.has( "DataChecksum" ) ? jsonData.getString( "DataChecksum" ) : "";
            strCreatedBy = jsonData.has( "CreatedBy" ) ? jsonData.getString( "CreatedBy" ) : "";;
            createdAtDate = jsonData.has( "CreatedAtDate" ) ? Utilities.parseLocalDate( jsonData.getString( "CreatedAtDate" ), Constants._ISO_Date_Format ) : null;
            createdAtTime = jsonData.has( "CreatedAtTime" ) ? Utilities.parseLocalTime( jsonData.getString( "CreatedAtTime" ), Constants._Time_Format_24 ) : null;
    		
            if ( Utilities.isNullOrEmpty( strDataChecksum ) ) {
            	
            	strDataChecksum = calcDataCheckSum();
            	
            }
            
            if ( Utilities.isNullOrEmpty( strCreatedBy ) ) {
            	
            	strCreatedBy = "process_log";
            	
            }

            if ( createdAtDate == null ) {
            
            	createdAtDate = LocalDate.now();
        		
        	}
            
            if ( createdAtTime == null ) {
            	
            	createdAtTime = LocalTime.now();
        	
            }

    	}
    	catch ( final Exception ex ) {
    		
    		//
    		
    	}
    	
    }
    
}
