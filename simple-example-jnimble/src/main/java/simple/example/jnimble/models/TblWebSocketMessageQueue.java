package simple.example.jnimble.models;

import java.time.LocalDateTime;

import net.nimble.annotations.Column;
import net.nimble.annotations.Id;
import net.nimble.annotations.Table;

@Table( "tblWebSocketMessageQueue" )
public class TblWebSocketMessageQueue {

	@Id
	@Column( "Id" )
	private String strId;

	@Column( "Type" )
	private String strType;

	@Column( "WorkerName" )
	private String strWorkerName;

	@Column( "CreatedAt" )
	private LocalDateTime createdAt;

	@Column( "Message" )
	private String strMessage;

	public String getId() {
		
		return strId;
		
	}

	public void setId( final String strId ) {
		
		this.strId = strId;
		
	}

	public String getType() {
		
		return strType;
		
	}

	public void setType( final String strType ) {
		
		this.strType = strType;
		
	}

	public String getWorkerName() {
		
		return strWorkerName;
		
	}

	public void setWorkerName( final String strWorkerName ) {
	
		this.strWorkerName = strWorkerName;
		
	}

	public String getMessage() {
		
		return strMessage;
		
	}

	public void setMessage( final String strMessage) {
		
		this.strMessage = strMessage;
		
	}

}
